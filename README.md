Setup:  
1. `python3.9 -m venv env`  
2. `source env/bin/activate`  
3. `python3 -m pip install -r requirements.txt`
4. Copy example config file `cp config-example.yaml config.yaml`) and update it
5. Run the example code with `python3 main.py`
