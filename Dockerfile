from python

workdir /usr/src/app
copy requirements.txt .
run python -m pip install -r requirements.txt

copy . .
cmd ["python", "main.py"]
