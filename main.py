import jwt
import jks
import json
import yaml
import base64
import asyncio
import aiohttp
import getpass
from pathlib import Path
from datetime import datetime, timedelta
from urllib.parse import urlencode, urlunparse, urlparse

from pprint import pprint

"""
Steps for setting this up from:  
https://help.salesforce.com/articleView?id=sf.remoteaccess_oauth_jwt_flow.htm&type=5
"""

ACCESS_TOKEN_PATH = Path.cwd() / "access_token"


async def authorize(client_id: str):
    """necessary to run this from the browser to confirm application permissions"""
    args = {
        "client_id": client_id,
        "redirect_uri": "https://login.salesforce.com/services/oauth2/success",
        "response_type": "code",
    }
    parts = list(urlparse("https://login.salesforce.com/services/oauth2/authorize"))
    parts[4] = urlencode(args)
    print(f"enter this url in your browser: {urlunparse(parts)}")
    input()


def create_jwt(headers, payload) -> str:
    """not sure how to do this with jwt library so reimlemented here"""
    f = lambda o: base64.urlsafe_b64encode(json.dumps(o).encode()).decode()
    jwt = ".".join(map(f, [headers, payload]))
    return jwt


def sign_jwt(jwt: str, pkey) -> str:
    """load keystore after downloading from sf. locked with account password"""
    signed = sign(pkey, jwt.encode(), "sha256")
    signature = base64.urlsafe_b64encode(signed).decode()
    return f"{jwt}.{signature}"


def get_cached_token():
    if ACCESS_TOKEN_PATH.is_file():
        with open(ACCESS_TOKEN_PATH, "r") as f:
            return f.read().strip()
    return None


async def get_token_from_password(config):
    access_token = get_cached_token()
    if access_token is not None:
        return access_token

    login_host = config["login_host"]
    client_id = config["client_id"]
    client_secret = config["client_secret"]
    username, password, security_token = [
        v if (v := config.get(k)) else getpass.getpass(f"{k}:")
        for k in ["username", "password", "security_token"]
    ]
    params = {
        "client_id": client_id,
        "client_secret": client_secret,
        "grant_type": "password",
        "username": username,
        "password": password + security_token,
    }
    async with aiohttp.ClientSession() as session:
        async with session.post(
            f"https://{login_host}/services/oauth2/token", params=params
        ) as response:
            assert response.status == 200, "Failed to get token!"
            payload = await response.json()

        _id, token_type, access_token = (
            payload["id"],
            payload["token_type"],
            payload["access_token"],
        )
        async with session.get(
            _id, headers={"Authorization": f"{token_type} {access_token}"}
        ) as response:
            assert response.status == 200, "Failed to get id payload!"
            payload = await response.json()
        display_name = payload["display_name"]

        with open(ACCESS_TOKEN_PATH, "w") as f:
            f.write(access_token)

        print(f"Saved new token for {display_name}")

        return access_token


async def get_token(config):
    """check for cached token "access_token" otherwise fetch new one"""
    access_token = get_cached_token()
    if access_token is not None:
        return access_token

    from OpenSSL.crypto import FILETYPE_ASN1, load_privatekey, sign

    # only necessary once per sf app
    await authorize(config["client_id"])

    headers = {"alg": "RS256"}
    payload = {
        "iss": config["client_id"],
        "aud": "https://login.salesforce.com",
        "sub": config["username"],
        "exp": int((datetime.utcnow() + timedelta(minutes=2)).timestamp()),
    }
    ks = jks.KeyStore.load("certs/keystore.jks", config["keystore_password"])
    pk_entry = next(iter(ks.private_keys.values()))
    pkey = load_privatekey(FILETYPE_ASN1, pk_entry.pkey)
    jwt = sign_jwt(create_jwt(headers, payload), pkey)

    async with aiohttp.ClientSession() as session:
        params = {
            "grant_type": "urn:ietf:params:oauth:grant-type:jwt-bearer",
            "assertion": jwt,
        }
        response = await session.post(
            f"{config['base_url']}/services/oauth2/token", params=params
        )
        if response.status != 200:
            print(await response.text())
            raise Exception("token retrieval failed")

        obj = await response.json()
        # also includes token_type, scope, instance_url, (user) id
        access_token = obj["access_token"]

        with open(ACCESS_TOKEN_PATH, "w") as f:
            f.write(access_token)

        return access_token


async def main(config):

    # token = await get_token(config)
    token = await get_token_from_password(config)
    headers = {"Authorization": f"Bearer {token}"}

    async with aiohttp.ClientSession(headers=headers) as session:

        async def f(path, query=None):
            async with session.get(config["base_url"] + path, params=query) as response:
                assert response.status == 200, f"fuck ({response.status})"
                payload = await response.json()
                return payload

        # root api versions (v50.0 is latest)
        # o = await f("/services/data/")

        # list available api endpoints
        # o = await f("/services/data/v50.0")

        # list account names
        o = await f("/services/data/v50.0/query/", {"q": "SELECT name from Account"})
        print(json.dumps(o, indent="  "))


if __name__ == "__main__":
    with open("config.yaml", "r") as f:
        try:
            config = yaml.safe_load(f)

            # retrieved from "Setup > My Domain"
            config["base_url"] = f"https://{config['instance']}.my.salesforce.com"

        except yaml.YAMLError as e:
            print("invalid config file")
            raise e

    try:
        # asyncio.run(get_token())
        asyncio.run(main(config))
    except KeyboardInterrupt:
        pass
